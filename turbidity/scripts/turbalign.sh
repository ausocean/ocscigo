#!/bin/bash

# This script is used to align a turbidity target in front of a Pi camera such 
# that the target occupies the bottom and leftmost 1% of the screen, with the
# edge between the white and black areas exactly in the middle of that 1% area.
# While this utility is running, the preview video output from the Pi camera
# must be visible on a monitor in real time while the turbidity target is moved
# and adjusted.

prompt ()
{
    echo "Press t for target subarea, f for full area, q to quit"
}

runfull ()
{
    raspivid -t 0 -p 100,100,1640,922 &
    echo "Place the target area in the bottom left corner of the screen, black on the right, then press t"
    prompt
}

runtarget ()
{
    raspivid -t 0 -roi 0.0,0.9,0.1,0.1 -a "|" -p 100,100,1640,922 -ae 160 &
    echo "Adjust the target to just fill the screen and align the bar with the edge between white and black"
    prompt
}

done=false
runfull
while [ $done == false ]; do
    IFS= read -s -n1 c
    case $c in
        "q" ) kill %+ ; done=true ;;
        "f" ) kill %+ ; runfull ;; 
        "t" ) kill %+ ; runtarget ;;
        * ) prompt ;;
    esac
done
