# Readme

ocscigo (pronounced "ox-i-go") is a collection of tools and packages written in Go for OCean SCIence.

turbalign.sh is a utility for aligning a turbidity target in front of a pi camera.

# License

Copyright (C) 2018 the Australian Ocean Lab (AusOcean).

It is free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

It is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
or more details.

You should have received a copy of the GNU General Public License
along with revid in gpl.txt. If not, see [GNU licenses](http://www.gnu.org/licenses/).